# Pony Maze - Help a pony escape from monster Domokun!

Play it here: https://pony-challenge-tvzshfkkdn.now.sh/

## Requirements
- NodeJS >7.x.x
- Yarn

## Running the project

Firstly, install project packages by running
```
yarn
```
and then you can launch it with:
```
yarn start
```

You can optionally use Vagrant or Docker for this project

## Building
If you want to create a production build of this project you can run
```
yarn build
```

## Linting

This project uses `TSLint` and `tslint-react` to lint our code.

You can use linter from CLI
```
yarn lint
```

autofixing
```
yarn autofix
```

## Testing
> "Always code as if the guy who ends up maintaining your code will be a violent psychopath who knows where you live." - Martin Golding

This project uses Jest for testing. To run test suite use:
```
yarn test
```

