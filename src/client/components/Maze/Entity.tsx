import { IStore } from 'client/store/state';
import * as React from 'react';
import { connect } from 'react-redux';
import * as style from './entity.scss';

export enum EntityType {
  Pony = 'pony',
  Domokun = 'domokun',
  EndPoint = 'endpoint'
}

export enum EntityDirection {
  Up = 'Up',
  Down = 'Down',
  Left = 'Left',
  Right = 'Right'
}

interface IOwnProps {
  type: EntityType;
}

interface IStateProps {
  direction: EntityDirection;
}

type Props = IOwnProps & IStateProps;

const Entity: React.SFC<Props> = ({ direction, type }) => (
  <div className={`${style.entity} ${style[type]} ${style[type + direction]}`} />
);

const mapStateToProps = (state: IStore, ownProps: IOwnProps): IStateProps => {
  switch (ownProps.type) {
    case EntityType.Pony:
      return {
        direction: state.maze.ponyDirection
      };
    case EntityType.Domokun:
      return {
        direction: state.maze.domokunDirection
      };
    case EntityType.EndPoint:
    default:
      return {
        direction: EntityDirection.Up
      };
  }
};

export default connect<IStateProps, {}, IOwnProps>(mapStateToProps)(Entity);
