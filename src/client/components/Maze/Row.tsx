import { Direction } from 'client/store/modules/maze/types';
import { IStore } from 'client/store/state';
import * as React from 'react';
import { connect } from 'react-redux';
import Entity, { EntityType } from './Entity';
import Tile from './Tile';

interface IStateProps {
  pony: number;
  domokun: number;
  endPoint: number;
  width: number;
  height: number;
}

interface IOwnProps {
  index: number;
  tiles: Direction[][];
}

type Props = IStateProps & IOwnProps;

const getEntityType = (key: number, domokun: number, pony: number, endpoint: number) => {
  if (key === domokun) {
    return EntityType.Domokun;
  }

  if (key === pony) {
    return EntityType.Pony;
  }

  if (key === endpoint) {
    return EntityType.EndPoint;
  }

  return;
};

const Row: React.SFC<Props> = ({ index, tiles, domokun, endPoint, pony, width, height }) => {
  return (
    <div>
      {tiles.map((walls, columnIndex) => {
        const key = (index * width) + columnIndex;
        const entityType = getEntityType(key, domokun, pony, endPoint);

        return (
          <Tile
            key={key}
            walls={{
              top: walls.indexOf(Direction.North) !== -1,
              right: columnIndex + 1 === width,
              bottom: index + 1 === height,
              left: walls.indexOf(Direction.West) !== -1
            }}
          >
            {entityType && <Entity type={entityType} />}
          </Tile>
        );
      })}
    </div>
  );
};

export default connect<IStateProps, {}, IOwnProps>((state: IStore) => {
  if (!state.maze.maze) {
    return {
      domokun: -1,
      endPoint: -1,
      height: 0,
      width: 0,
      pony: -1
    };
  }

  const { domokun, endPoint, pony, size } = state.maze.maze;

  return {
    domokun: domokun[0],
    endPoint: endPoint[0],
    pony: pony[0],
    height: size[1],
    width: size[0]
  };
})(Row);
