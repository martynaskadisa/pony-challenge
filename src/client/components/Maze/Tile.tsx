import { Direction } from 'client/store/modules/maze/types';
import * as React from 'react';
import * as style from './tile.scss';

interface IWalls {
  top?: boolean;
  right?: boolean;
  bottom?: boolean;
  left?: boolean;
}

interface IProps {
  walls: IWalls;
}

const getClassNames = (walls: IWalls) => {
  const classNames = [
    style.tile,
    walls.top && style.tileBorderTop,
    walls.right && style.tileBorderRight,
    walls.bottom && style.tileBorderBottom,
    walls.left && style.tileBorderLeft
  ];

  return classNames.join(' ');
};

const Tile: React.SFC<IProps> = ({ children, walls }) => (
  <div className={getClassNames(walls)}>
    {children}
  </div>
);

export default Tile;
