import { Direction } from 'client/store/modules/maze/types';
import { IStore } from 'client/store/state';
import * as React from 'react';
import { connect } from 'react-redux';
import Entity, { EntityType } from './Entity';
import Row from './Row';
import Tile from './Tile';
import * as style from './style.scss';

interface IStateProps {
  isReady: boolean;
  data: Direction[][];
  size: number[];
}

type Props = IStateProps;

const CELL_SIZE = 30;

const Maze: React.SFC<Props> = ({ isReady, data, size }) => {
  if (!isReady) {
    return <div>No data!</div>;
  }

  const rows: Direction[][][] = [];

  for (let i = 0; i < size[1]; i++) {
    const row = data.slice(i * size[0], (i + 1) * size[0]);
    rows.push(row);
  }

  return (
    <div className={style.container} style={{ width: `${size[0] * CELL_SIZE}px` }}>
      {rows.map((tiles, index) => (
        <Row key={index} index={index} tiles={tiles} />
      ))}
    </div>
  );
};

export default connect<IStateProps, {}, {}>((state: IStore) => {
  if (!state.maze.maze) {
    return {
      isReady: false,
      size: [],
      data: []
    };
  }

  return {
    isReady: true,
    data: state.maze.maze.data,
    size: state.maze.maze.size
  };
})(Maze);
