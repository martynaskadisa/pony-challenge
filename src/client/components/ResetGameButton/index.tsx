import { resetGame } from 'client/store/modules/maze/actions';
import * as React from 'react';
import { connect } from 'react-redux';
import * as style from './style.scss';

interface IDispatchProps {
  onClick: () => any;
}

const ResetGameButton: React.SFC<IDispatchProps> = ({ onClick }) => (
  <button className={style.button} onClick={onClick}>
    Play again
  </button>
);

export default connect<{}, IDispatchProps, {}>(null, (dispatch) => ({
  onClick: () => dispatch(resetGame())
}))(ResetGameButton);
