import Error from 'client/components/Error';
import Loading from 'client/components/Loading';
import * as React from 'react';
import { Helmet } from 'react-helmet';
import createLazyContainer from 'react-lazy-import';
import { Route, RouteComponentProps, Switch } from 'react-router-dom';
import './normalize.scss';
import * as style from './style.scss';

const Home = createLazyContainer<RouteComponentProps<{}>>(
  () => import(
    /* webpackChunkName: 'home' */
    'client/scenes/Home'
  ), Loading, Error
);
const Game = createLazyContainer<RouteComponentProps<{}>>(
  () => import(
    /* webpackChunkName: 'game' */
    'client/scenes/Game'
  ), Loading, Error
);
const NotFound = createLazyContainer<RouteComponentProps<{}>>(
  () => import(
    /* webpackChunkName: 'not-found' */
    'client/scenes/NotFound'
  ), Loading, Error
);
const Won = createLazyContainer<RouteComponentProps<{}>>(
  () => import(
    /* webpackChunkName: 'won' */
    'client/scenes/Won'
  ), Loading, Error
);
const Lost = createLazyContainer<RouteComponentProps<{}>>(
  () => import(
    /* webpackChunkName: 'lost' */
    'client/scenes/Lost'
  ), Loading, Error
);

interface IState {
  hasError: boolean;
}

class App extends React.Component<{}, IState> {
  public state: IState = {
    hasError: false
  };

  public componentDidCatch () {
    this.setState({ hasError: true });
  }

  public render () {
    if (this.state.hasError) {
      return (
        <div>
          Whoops! This app broke :(
        </div>
      );
    }

    return (
      <div className={style.appShell}>
        <Helmet
          titleTemplate='%s - Pony Maze'
          defaultTitle='Pony Maze'
        />

        <Switch>
          <Route exact path='/' component={Home} />
          <Route path='/maze' component={Game} />
          <Route path='/won' component={Won} />
          <Route path='/lost' component={Lost} />
          <Route component={NotFound} />
        </Switch>
      </div>
    );
  }
}

export default App;
