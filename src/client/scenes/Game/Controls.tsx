import { movePony } from 'client/store/modules/maze/actions';
import { Direction } from 'client/store/modules/maze/types';
import * as React from 'react';
import { connect } from 'react-redux';
import * as style from './controls.scss';

interface IProps {
  onUpClick: () => any;
  onRightClick: () => any;
  onDownClick: () => any;
  onLeftClick: () => any;
  onSpaceClick: () => any;
}

interface IState {
  isUpButtonActive: boolean;
  isDownButtonActive: boolean;
  isLeftButtonActive: boolean;
  isRightButtonActive: boolean;
  isSpaceButtonActive: boolean;
}

const TIMEOUT = 50;

class Controls extends React.Component<IProps, IState> {
  public state: IState = {
    isDownButtonActive: false,
    isLeftButtonActive: false,
    isRightButtonActive: false,
    isUpButtonActive: false,
    isSpaceButtonActive: false
  };

  constructor () {
    super();

    this.handleKeyPress = this.handleKeyPress.bind(this);
  }

  public componentDidMount () {
    window.addEventListener('keydown', this.handleKeyPress);
  }

  public componentWillUnmount () {
    window.removeEventListener('keydown', this.handleKeyPress);
  }

  public render () {
    const { onDownClick, onLeftClick, onRightClick, onUpClick, onSpaceClick } = this.props;
    const {
      isDownButtonActive,
      isLeftButtonActive,
      isRightButtonActive,
      isUpButtonActive,
      isSpaceButtonActive
    } = this.state;

    return (
      <div className={style.container}>
        <div className={style.row}>
          <button
            onClick={onUpClick}
            className={`${style.button} ${style.up} ${isUpButtonActive ? style.buttonActive : ''}`}
          />
        </div>
        <div className={style.row}>
          <button
            onClick={onLeftClick}
            className={`${style.button} ${style.left} ${isLeftButtonActive ? style.buttonActive : ''}`}
          />
          <button
            onClick={onDownClick}
            className={`${style.button} ${style.down} ${isDownButtonActive ? style.buttonActive : ''}`}
          />
          <button
            onClick={onRightClick}
            className={`${style.button} ${style.right} ${isRightButtonActive ? style.buttonActive : ''}`}
          />
        </div>
        <div className={style.row}>
          <button
            onClick={onSpaceClick}
            className={`${style.button} ${style.space} ${isSpaceButtonActive ? style.buttonActive : ''}`}
          >
            Stay (Space)
          </button>
        </div>
      </div>
    );
  }

  private handleKeyPress (e: KeyboardEvent): void {
    const { onDownClick, onLeftClick, onRightClick, onUpClick, onSpaceClick } = this.props;

    switch (e.key) {
      case 'ArrowUp':
      case 'w':
        this.animateButton('isUpButtonActive');
        onUpClick();
        break;
      case 'ArrowDown':
      case 's':
        this.animateButton('isDownButtonActive');
        onDownClick();
        break;
      case 'ArrowLeft':
      case 'a':
        this.animateButton('isLeftButtonActive');
        onLeftClick();
        break;
      case 'ArrowRight':
      case 'd':
        this.animateButton('isRightButtonActive');
        onRightClick();
        break;
      case ' ':
        this.animateButton('isSpaceButtonActive');
        onSpaceClick();
        break;
      default:
        break;
    }
  }

  private animateButton (key: string): void {
    this.setState(
      { [key]: true } as any,
      () => setTimeout(
        () => this.setState({ [key]: false } as any), TIMEOUT
      )
    );
  }
}

export default connect<{}, IProps, {}>(null, (dispatch) => ({
  onUpClick: () => dispatch(movePony(Direction.North)),
  onDownClick: () => dispatch(movePony(Direction.South)),
  onLeftClick: () => dispatch(movePony(Direction.West)),
  onRightClick: () => dispatch(movePony(Direction.East)),
  onSpaceClick: () => dispatch(movePony(Direction.Stay))
}))(Controls);
