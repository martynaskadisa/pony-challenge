import Maze from 'client/components/Maze';
import { fetchMaze } from 'client/store/modules/maze/actions';
import { GameState } from 'client/store/modules/maze/types';
import { IStore } from 'client/store/state';
import * as React from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { Redirect, RouteComponentProps } from 'react-router-dom';
import Controls from './Controls';

interface IStateProps {
  hasMazeId: boolean;
  hasWon: boolean;
  hasLost: boolean;
}

interface IDispatchProps {
  onMount: () => any;
}

type OwnProps = RouteComponentProps<{}>;

type Props = IStateProps & IDispatchProps & OwnProps;

class Game extends React.Component<Props, {}> {
  public componentWillMount () {
    const { onMount } = this.props;

    onMount();
  }

  public render () {
    const { hasMazeId, hasLost, hasWon } = this.props;

    if (!hasMazeId) {
      return <Redirect to='/' />;
    }

    if (hasLost) {
      return <Redirect to='/lost' />;
    }

    if (hasWon) {
      return <Redirect to='/won' />;
    }

    return (
      <div>
        <Helmet>
          <title>Game</title>
        </Helmet>
        <Maze />
        <Controls />
      </div>
    );
  }
}

export default connect<IStateProps, IDispatchProps, OwnProps>(({ maze }: IStore) => ({
    hasMazeId: !!maze.mazeId,
    hasLost: maze.maze
      ? maze.maze.gameState.state === GameState.Lost
      : false,
    hasWon: maze.maze
      ? maze.maze.gameState.state === GameState.Won
      : false
}), (dispatch) => ({
  onMount: () => dispatch(fetchMaze())
}))(Game);
