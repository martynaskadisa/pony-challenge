import { IStore } from 'client/store/state';
import * as React from 'react';
import { connect } from 'react-redux';
import * as style from './errorAlert.scss';

interface IStateProps {
  error: string | null;
}

const ErrorAlert: React.SFC<IStateProps> = ({ error }) => {
  if (!error) {
    return null;
  }

  return (
    <div className={style.container}>{error}</div>
  );
};

export default connect<IStateProps, {}, {}>((state: IStore) => ({
  error: state.maze.error
}))(ErrorAlert);
