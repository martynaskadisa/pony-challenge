import { createNewMaze } from 'client/store/modules/maze/actions';
import { ICreateNewMazePayload } from 'client/store/modules/maze/types';
import * as React from 'react';
import { connect } from 'react-redux';
import ErrorAlert from './ErrorAlert';
import * as style from './form.scss';

const MIN_DIMENSION = 15;
const MAX_DIMENSION = 25;
const MIN_DIFFICULTY = 0;
const MAX_DIFFICULTY = 10;

interface IDispatchProps {
  onSubmit: (data: any) => any;
}

type Props = IDispatchProps;
type State = ICreateNewMazePayload;

class Form extends React.Component<Props, State> {
  public state: State = {
    width: MIN_DIMENSION,
    height: MIN_DIMENSION,
    playerName: '',
    difficulty: 0
  };

  constructor () {
    super();

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  public render () {
    const { width, height, playerName, difficulty } = this.state;

    return (
      <div className={style.container}>
        <form onSubmit={this.handleSubmit} className={style.form}>
          <h2 className={style.heading}>Start game</h2>
          <ErrorAlert />
          <label className={style.labelContainer}>
            <span className={style.label}>Maze width:</span>
            <input
              type='number'
              min={MIN_DIMENSION}
              max={MAX_DIMENSION}
              value={width}
              className={style.input}
              onChange={(e) => this.handleChange(e, 'width', 'number')}
            />
          </label>
          <label className={style.labelContainer}>
            <span className={style.label}>Maze height:</span>
            <input
              type='number'
              min={MIN_DIMENSION}
              max={MAX_DIMENSION}
              value={height}
              className={style.input}
              onChange={(e) => this.handleChange(e, 'height', 'number')}
            />
          </label>
          <label className={style.labelContainer}>
            <span className={style.label}>Player name:</span>
            <input
              type='text'
              placeholder='E.g. Rainbow Dash'
              value={playerName}
              className={style.input}
              onChange={(e) => this.handleChange(e, 'playerName', 'string')}
            />
          </label>
          <label className={style.labelContainer}>
            <span className={style.label}>Difficulty</span>
            <select
              value={difficulty}
              className={style.input}
              onChange={(e) => this.handleChange(e, 'difficulty', 'number')}
            >
              {Array(MAX_DIFFICULTY + 1).join('.').split('.').map((empty, index) => (
                <option
                  key={index}
                  value={index}
                >
                  {index}
                </option>
              ))}
            </select>
          </label>
          <input type='submit' value='Play' className={style.button} />
        </form>
      </div>
    );
  }

  private handleChange (
    e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>,
    key: string,
    dataType: 'string' | 'number'
  ) {
    let value: number | string = e.target.value;

    if (dataType === 'number') {
      value = parseInt(value, 10);
    }

    this.setState({ [key]: value } as any);
  }

  private handleSubmit (e: React.FormEvent<any>) {
    e.preventDefault();
    this.props.onSubmit(this.state);
  }
}

export default connect<{}, IDispatchProps, {}>(null, (dispatch) => ({
  onSubmit: (payload: any) => dispatch(createNewMaze(payload))
}))(Form);
