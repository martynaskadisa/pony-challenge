import * as React from 'react';
import * as style from './header.scss';

const Header: React.SFC<{}> = () => (
  <div className={style.heading}>
    My little pony<br/> maze
  </div>
);

export default Header;
