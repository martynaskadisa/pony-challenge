import { IStore } from 'client/store/state';
import * as React from 'react';
import { connect } from 'react-redux';
import { Redirect, RouteComponentProps } from 'react-router-dom';
import Form from './Form';
import Header from './Header';

interface IStateProps {
  hasMazeId: boolean;
}

type OwnProps = RouteComponentProps<{}>;

type Props = IStateProps & OwnProps;

const Home: React.SFC<Props> = ({ hasMazeId }) => {
  if (hasMazeId) {
    return <Redirect to={`/maze`} />;
  }

  return (
    <div>
      <Header />
      <Form />
    </div>
  );
};

export default connect<IStateProps, {}, OwnProps>((state: IStore, ownProps) => ({
  hasMazeId: !!state.maze.mazeId
}))(Home);
