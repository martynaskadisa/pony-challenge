import ResetGameButton from 'client/components/ResetGameButton';
import { GameState } from 'client/store/modules/maze/types';
import { IStore } from 'client/store/state';
import * as React from 'react';
import { connect } from 'react-redux';
import { Link, Redirect, RouteComponentProps } from 'react-router-dom';
import * as style from './style.scss';

interface IStateProps {
  mazeExists: boolean;
  hasLost: boolean;
  imgUrl?: string;
}

type OwnProps = RouteComponentProps<{}>;

type Props = IStateProps & OwnProps;

const Lost: React.SFC<Props> = ({ mazeExists, hasLost, imgUrl }) => {
  if (!mazeExists) {
    return <Redirect to='/' />;
  }

  if (!hasLost) {
    return <Redirect to='/maze' />;
  }

  return (
    <div className={style.container}>
      <h1>You lost!</h1>
      <img src={imgUrl} style={{ width: '100%' }} />
      <ResetGameButton />
    </div>
  );
};

export default connect<IStateProps, {}, OwnProps>(({ maze }: IStore) => {
  if (!maze.maze) {
    return {
      mazeExists: false,
      hasLost: false
    };
  }

  return {
    hasLost: maze.maze.gameState.state === GameState.Lost,
    mazeExists: true,
    imgUrl: maze.maze.gameState.hiddenUrl
  };
})(Lost);
