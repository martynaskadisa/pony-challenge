import ResetGameButton from 'client/components/ResetGameButton';
import { GameState, Status } from 'client/store/modules/maze/types';
import { IStore } from 'client/store/state';
import * as React from 'react';
import { connect } from 'react-redux';
import { Link, Redirect, RouteComponentProps } from 'react-router-dom';
import * as style from './style.scss';

interface IStateProps {
  mazeExists: boolean;
  hasWon: boolean;
  imgUrl?: string;
}

type OwnProps = RouteComponentProps<{}>;

type Props = IStateProps & OwnProps;

const Won: React.SFC<Props> = ({ mazeExists, hasWon, imgUrl }) => {
  if (!mazeExists) {
    return <Redirect to='/' />;
  }

  if (!hasWon) {
    return <Redirect to='/maze' />;
  }

  return (
    <div className={style.container}>
      <h1>Winner!</h1>
      <img src={imgUrl} style={{ width: '100%' }} />
      <ResetGameButton />
    </div>
  );
};

export default connect<IStateProps, {}, OwnProps>(({ maze }: IStore) => {
  if (!maze.maze) {
    return {
      mazeExists: false,
      hasWon: false
    };
  }

  return {
    hasWon: maze.maze.gameState.state === GameState.Won,
    mazeExists: true,
    imgUrl: maze.maze.gameState.hiddenUrl
  };
})(Won);
