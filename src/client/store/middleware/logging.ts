// tslint:disable:no-console
import { Middleware } from 'redux';

const INDENTATION = 2;

const logger: Middleware = store => next => action => {
  console.group(action.type);
  console.info('Dispatching:');
  console.table(JSON.parse(JSON.stringify(action, undefined, INDENTATION)));

  const result = next(action);

  console.info('Next state:');
  console.table(JSON.parse(JSON.stringify(store.getState(), undefined, INDENTATION)));
  console.groupEnd();

  return result;
};

export default logger;
