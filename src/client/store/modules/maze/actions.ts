import { createAction, ThunkAction } from 'client/store/action';
import {
  CREATE_NEW_MAZE_FAIL,
  CREATE_NEW_MAZE_START,
  CREATE_NEW_MAZE_SUCCESS,
  FETCH_MAZE_FAIL,
  FETCH_MAZE_START,
  FETCH_MAZE_SUCCESS,
  MOVE_PONY_FAIL,
  MOVE_PONY_START,
  MOVE_PONY_SUCCESS,
  RESET_GAME
} from './constants';
import {
  Direction,
  ICreateMazeError,
  ICreateMazePayload,
  ICreateMazeSuccess,
  ICreateNewMazePayload,
  IFetchMazeSuccess,
  IFetchMazeSuccessData,
  IMaze,
  IMovePonyError,
  IMovePonyPayload,
  IMovePonySuccess,
  IState,
  Status
} from './types';

const createMazeStart = createAction<never>(CREATE_NEW_MAZE_START);
const createMazeSuccess = createAction<string>(CREATE_NEW_MAZE_SUCCESS);
const createMazeFail = createAction<string>(CREATE_NEW_MAZE_FAIL);
const resetGameAction = createAction(RESET_GAME);

export const resetGame = (): ThunkAction => (dispatch, getState) => {
  dispatch(resetGameAction());

  window.history.pushState(undefined, '', '/');
};

export const createNewMaze = ({ width, height, playerName, difficulty }: ICreateNewMazePayload): ThunkAction =>
  (dispatch, getState, request) => {
    dispatch(createMazeStart());

    request
      .post('/maze', {
        'maze-width': width,
        'maze-height': height,
        'maze-player-name': playerName,
        'difficulty': difficulty
      } as ICreateMazePayload)
      .then((res: ICreateMazeSuccess) => dispatch(createMazeSuccess(res.data.maze_id)))
      .catch((err: any) => {
        const res: ICreateMazeError = err.response;

        dispatch(createMazeFail(res.data ? res.data : 'Unspecified error'));
      });
  };

const fetchMazeStart = createAction(FETCH_MAZE_START);
const fetchMazeSuccess = createAction<IMaze>(FETCH_MAZE_SUCCESS);
const fetchMazeFail = createAction<string>(FETCH_MAZE_FAIL);

const mapFetchMazeResponse = ({ data }: IFetchMazeSuccess): IMaze => ({
  id: data.maze_id,
  pony: data.pony,
  domokun: data.domokun,
  size: data.size,
  gameState: {
    hiddenUrl: data['game-state']['hidden-url']
      ? 'https://ponychallenge.trustpilot.com' + data['game-state']['hidden-url']
      : undefined,
    state: data['game-state'].state,
    stateResult: data['game-state']['state-result']
  },
  difficulty: data.difficulty,
  data: data.data,
  endPoint: data['end-point']
});

export const fetchMaze = (): ThunkAction => (dispatch, getState, request): void => {
  const { mazeId } = getState().maze;

  if (!mazeId) {
    return;
  }

  dispatch(fetchMazeStart());

  request
    .get(`/maze/${mazeId}`)
    .then(mapFetchMazeResponse)
    .then((maze) => dispatch(fetchMazeSuccess(maze)))
    .catch((err: any) => {
      const res = err.response;

      dispatch(createMazeFail(res.data ? res.data : 'Unspecified error'));
    });
};

const movePonyStart = createAction(MOVE_PONY_START);
const movePonySuccess = createAction(MOVE_PONY_SUCCESS);
const movePonyFail = createAction(MOVE_PONY_FAIL);

export const movePony = (direction: Direction): ThunkAction => (dispatch, getState, request) => {
  const { mazeId, status } = getState().maze;

  if (
    !mazeId
    || status === Status.Moving
    || status === Status.Loading
    || status === Status.Over
  ) {
    return;
  }

  dispatch(movePonyStart());

  request
    .post(`/maze/${mazeId}`, { direction } as IMovePonyPayload)
    .then((res: IMovePonySuccess) => {
      dispatch(movePonySuccess());
      dispatch(fetchMaze());
    })
    .catch((err: any) => dispatch(movePonyFail()));
};
