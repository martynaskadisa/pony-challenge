export const CREATE_NEW_MAZE_START = 'maze/CREATE_NEW_MAZE_START';
export const CREATE_NEW_MAZE_SUCCESS = 'maze/CREATE_NEW_MAZE_SUCCESS';
export const CREATE_NEW_MAZE_FAIL = 'maze/CREATE_NEW_MAZE_FAIL';

export const FETCH_MAZE_START = 'maze/FETCH_MAZE_START';
export const FETCH_MAZE_SUCCESS = 'maze/FETCH_MAZE_SUCCESS';
export const FETCH_MAZE_FAIL = 'maze/FETCH_MAZE_FAIL';

export const MOVE_PONY_START = 'maze/MOVE_PONY_START';
export const MOVE_PONY_SUCCESS = 'maze/MOVE_PONY_SUCCESS';
export const MOVE_PONY_FAIL = 'maze/MOVE_PONY_FAIL';

export const RESET_GAME = 'maze/RESET_GAME';
