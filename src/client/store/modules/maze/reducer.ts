import { EntityDirection } from 'client/components/Maze/Entity';
import { Reducer } from 'redux';
import {
  CREATE_NEW_MAZE_FAIL,
  CREATE_NEW_MAZE_START,
  CREATE_NEW_MAZE_SUCCESS,
  FETCH_MAZE_FAIL,
  FETCH_MAZE_START,
  FETCH_MAZE_SUCCESS,
  MOVE_PONY_FAIL,
  MOVE_PONY_START,
  MOVE_PONY_SUCCESS,
  RESET_GAME
} from './constants';
import { GameState, IMaze, IState, Status } from './types';

const defaultState: IState = {
  status: Status.Idle,
  mazeId: null,
  error: null,
  maze: null,
  domokunDirection: EntityDirection.Up,
  ponyDirection: EntityDirection.Up
};

const isOver = (maze: IMaze) => [GameState.Lost, GameState.Won].indexOf(maze.gameState.state) !== -1;

const getDirection = (prevPos: number, nextPos: number, prevDirection: EntityDirection) => {
  if (prevPos === nextPos) {
    return prevDirection;
  }

  if (prevPos - nextPos === 1) {
    return EntityDirection.Left;
  }

  if (prevPos - nextPos === -1) {
    return EntityDirection.Right;
  }

  if (prevPos - nextPos > 1) {
    return EntityDirection.Up;
  }

  return EntityDirection.Down;
};

const reducer: Reducer<IState> = (state = defaultState, action) => {
  switch (action.type) {
    case RESET_GAME:
      return { ...defaultState };
    case FETCH_MAZE_START:
    case CREATE_NEW_MAZE_START:
      return {
        ...state,
        status: Status.Loading
      };
    case CREATE_NEW_MAZE_SUCCESS:
      return {
        ...state,
        status: Status.Idle,
        mazeId: action.payload,
        error: null
      };
    case FETCH_MAZE_FAIL:
    case CREATE_NEW_MAZE_FAIL:
      return {
        ...state,
        status: Status.Error,
        error: action.payload
      };
    case FETCH_MAZE_SUCCESS:
      return {
        ...state,
        status: isOver(action.payload)
          ? Status.Over
          : Status.Idle,
        error: null,
        maze: action.payload,
        mazeId: action.payload.id,
        domokunDirection: state.maze
          ? getDirection(state.maze.domokun[0], action.payload.domokun[0], state.domokunDirection)
          : EntityDirection.Up,
        ponyDirection: state.maze
          ? getDirection(state.maze.pony[0], action.payload.pony[0], state.ponyDirection)
          : EntityDirection.Up
      };
    case MOVE_PONY_START:
      return {
        ...state,
        status: Status.Moving
      };
    case MOVE_PONY_FAIL:
    case MOVE_PONY_SUCCESS:
      return {
        ...state,
        status: Status.Idle
      };
    default:
      return state;
  }
};

export default reducer;
