import { AxiosResponse } from 'axios';
import { EntityDirection } from 'client/components/Maze/Entity';
import { IMaze } from './types';

export enum Status {
  Idle = 'idle',
  Loading = 'loading',
  Error = 'error',
  Moving = 'moving',
  Over = 'over'
}

export interface IState {
  mazeId: string | null;
  status: Status;
  error: string | null;
  maze: IMaze | null;
  ponyDirection: EntityDirection;
  domokunDirection: EntityDirection;
}

export interface ICreateMazePayload {
  'maze-width': number;
  'maze-height': number;
  'maze-player-name': string;
  'difficulty': number;
}

export interface ICreateMazeSuccess extends AxiosResponse {
  data: {
    maze_id: string;
  };
}

export interface ICreateMazeError extends AxiosResponse {
  data: string;
}

export interface ICreateNewMazePayload {
  width: number;
  height: number;
  playerName: string;
  difficulty: number;
}

export enum Direction {
  North = 'north',
  East = 'east',
  South = 'south',
  West = 'west',
  Stay = 'stay'
}

export enum GameState {
  Active = 'Active',
  Won = 'won',
  Lost = 'over'
}

export interface IFetchMazeSuccessData {
  'pony': number[];
  'domokun': number[];
  'end-point': number[];
  'size': number[];
  'difficulty': number;
  'data': Direction[][];
  'maze_id': string;
  'game-state': {
    'hidden-url'?: string;
    'state': GameState,
    'state-result': string;
  };
}

export interface IMaze {
  pony: number[];
  domokun: number[];
  endPoint: number[];
  size: number[];
  difficulty: number;
  data: Direction[][];
  id: string;
  gameState: {
    hiddenUrl?: string;
    state: GameState,
    stateResult: string;
  };
}

export interface IFetchMazeSuccess extends AxiosResponse {
  data: IFetchMazeSuccessData;
}

export interface IMovePonyPayload {
  direction: Direction;
}

export interface IMovePonySuccess extends AxiosResponse {
  data: {
    'state': string;
    'state-result': string;
  };
}

export interface IMovePonyError extends AxiosResponse {
  data: string;
}
