import { combineReducers } from 'redux';
import mazeReducer from './modules/maze/reducer';
import { IStore } from './state';

const reducer = combineReducers<IStore>({
  maze: mazeReducer
});

export default reducer;

