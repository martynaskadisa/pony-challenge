import { IState as IMazeState } from './modules/maze/types';

export interface IStore {
  readonly maze: IMazeState;
}
