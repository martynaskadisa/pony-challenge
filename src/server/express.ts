import * as Express from 'express';
import * as helmet from 'helmet';
import * as logger from 'morgan';
import * as path from 'path';
import * as Raven from 'raven';
import frontendApp from './frontend';
import { isProd } from './utils';

const app = Express();

if (isProd && JSON.parse(process.env.IS_SENTRY_SERVER_ENABLED)) {
  Raven.config(process.env.SENTRY_DSN_SERVER).install();
  app.use(Raven.requestHandler());
}

if (!isProd) {
  app.use(logger('dev'));
}

app.use(helmet());

app.use('/', frontendApp);

if (isProd && JSON.parse(process.env.IS_SENTRY_SERVER_ENABLED)) {
  app.use(Raven.errorHandler());
}

export default app;
