import * as path from 'path';

export const ROOT_PATH = path.resolve(__dirname, '../../');

export const isProd = process.env.NODE_ENV === 'production';
