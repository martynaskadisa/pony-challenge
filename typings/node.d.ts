declare namespace NodeJS {
    interface ProcessEnv {
      IS_WEBPACK_STATS_ENABLED: string;
      NODE_ENV: string;
    }
}
